# Docker
時請先確保安裝下列套件
* [**Dokcer for mac (Edge 版本)**](https://store.docker.com/editions/community/docker-ce-desktop-mac)

已經寫成 bash 腳本方便操作 docker 指令:
>bash docker

或是懶惰使用 linux alias 來縮短指令, 例如將 dk 綁定 bash docker
1. 開啟 terminal

1. 確認這個 docker 專案的路徑
>$ pwd
/Applications/MAMP/www/docker

3. 將路徑取代到指令 cd 之後的路徑 (本機使用 bash 請指定到 .bashrc)
>echo "alias dk='cd /Applications/MAMP/www/docker && bash docker'" >> ~/.zshrc
4. 查看指令是否綁定成功
>dk


## Sell Script Helper
docker container 的啟動可以使用
1. docker
2. dokcer-compose

上述啟動方式因為指令太過繁雜故包裝成 shell 檔方便使用
>詳細指令請查看
>bash docker

![](https://i.imgur.com/kL33pJT.jpg)



## Service
service container 名稱會根據 `docker-compose.yml` 命名


### os
工作時使用的 container
* zsh 主題設定
>更改 .env 設定檔案 ZSH_THEME 變數
* Linux alias 綁定
>詳見 os/alias/.aliasrc.example
* crontab
>修改 os/crontab/cron 檔案
>(若已啟動請重啟 container 或直接在 container 內修改也可)

其餘資訊
* Composer
>Composer version 1.7.3

* PHP 7.2
>PHP 7.2.12 (cli) (built: Nov  6 2018 16:40:25) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.12, Copyright (c) 1999-2018, by Zend Technologies

<hr>

### nginx

搭配 php-fpm 啟動
* SSL (還在測試...)
>啟動後會自動產生 ssl key 可設定用於本機 https 開發上
* conf
>範例檔可參考 nginx/sites/default.conf.example
>已有含 cors 設定
其餘資訊
* nginx
>nginx 1.15.6
<hr>


### mariadb

* 環境設定 env
>MYSQL_DATABASE = 預設資料庫名稱
MYSQL_ROOT_PASSWORD = 預設 root 密碼
* 第一次加載
>第一次啟動會加載 mariadb/initial 底下的 .sql 檔案
>可用初始於創建資料庫
* 資料庫綁定
>將 container 內資料庫綁定外部 mariadb/database
>container 關閉後資料仍不會遺失


其餘資訊
* mariadb
>mariadb 10.1.23
<hr>

### node
node js application
基本上需要 compiler js 時再打開就好
好比 lumen 開發 api 時就可以在 `docker-compose.yml` 中 mark 起來再執行
`bash docker up` 啟動 container 以節省資源

* compiler || install
1. 新增一個 terminal 視窗
2. 切至 node container 內部
>bash docker in node
3. 進入專案資料夾
>cd ininder.laravel.admin
4. 安裝
>npm run install
5. compiler
>npm run watch

其餘資訊
* nodejs
>node v10.5.0
>npm v6.1.0


