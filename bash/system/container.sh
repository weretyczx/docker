#!/bin/bash
container.exec(){
    /usr/local/bin/docker-compose exec ${1} zsh -c "${2}"
}