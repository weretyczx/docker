#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # #
# black
# # # # # # # # # # # # # # # # # # # # # # #
color.text.black () {
    printf "$(tput setaf 0)${1}$(tput sgr0)"
}
color.background.black () {
    printf "$(tput setab 0)
    ${1}
    $(tput setab 0)$(tput sgr0)"
}

# # # # # # # # # # # # # # # # # # # # # # #
# red
# # # # # # # # # # # # # # # # # # # # # # #
color.text.red () {
    printf "$(tput setaf 1)${1}$(tput sgr0)"
}
color.background.red () {
    printf "$(tput setab 1)
    ${1}
    $(tput setab 1) $(tput sgr0)"
}

# # # # # # # # # # # # # # # # # # # # # # #
# green
# # # # # # # # # # # # # # # # # # # # # # #
color.text.green () {
    printf "$(tput setaf 2)${1}$(tput sgr0)"
}
color.background.green () {
    printf "$(tput setab 2)
    ${1}
    $(tput setab 2) $(tput sgr0)"
}

# # # # # # # # # # # # # # # # # # # # # # #
# yellow
# # # # # # # # # # # # # # # # # # # # # # #
color.text.yellow () {
    printf "$(tput setaf 3)${1}$(tput sgr0)"
}
color.background.yellow () {
    printf "$(tput setab 3)
    ${1}
    $(tput setab 3) $(tput sgr0)"
}

# # # # # # # # # # # # # # # # # # # # # # #
# blue
# # # # # # # # # # # # # # # # # # # # # # #
color.text.blue () {
    printf "$(tput setaf 4)${1}$(tput sgr0)"
}
color.background.blue () {
    printf "$(tput setab 4)
    ${1}
    $(tput setab 4) $(tput sgr0)"
}

# # # # # # # # # # # # # # # # # # # # # # #
# magenta
# # # # # # # # # # # # # # # # # # # # # # #
color.text.magenta () {
    printf "$(tput setaf 5)${1}$(tput sgr0)"
}
color.background.magenta () {
    printf "$(tput setab 5)
    ${1}
    $(tput setab 5) $(tput sgr0)"
}

# # # # # # # # # # # # # # # # # # # # # # #
# cyan
# # # # # # # # # # # # # # # # # # # # # # #
color.text.cyan () {
    printf "$(tput setaf 6)${1}$(tput sgr0)"
}
color.background.cyan () {
    printf "$(tput setab 6)
    ${1}
    $(tput setab 6) $(tput sgr0)"
}

# # # # # # # # # # # # # # # # # # # # # # #
# white
# # # # # # # # # # # # # # # # # # # # # # #
color.text.white () {
    printf "$(tput setaf 7)${1}$(tput sgr0)"
}
color.background.white () {
    printf "$(tput setab 7)
    ${1}
    $(tput setab 7) $(tput sgr0)"
}

