#!/bin/bash
source $ROOT/bash/system/container.sh

docker.check.engine(){
    # 確認 docker 是否開啟
    if [[ $(docker) -eq 0 ]]; then
        echo " $(color.background.red "$(color.text.black "Error: Docker engine is not ready. ")" )"
        exit
    fi
}
docker.up(){
    docker-sync start

    /usr/local/bin/docker-compose up -d "${@}"

    if [[ "${@}" =~ "$OS_CONTAINER" ]] || [ -z "${@}" ]; then
         # Setting theme
        container.exec $OS_CONTAINER "sed -i '/ZSH_THEME/s/=.*/=$ZSH_THEME/g' ~/.zshrc"
        # active cron job
        container.exec $OS_CONTAINER "crond && crontab /etc/cron.d/cron"

        container.exec $OS_CONTAINER "echo '. ~/alias/.aliasrc' >> ~/.zshrc && source ~/.zshrc"

    fi
}

docker.down(){
    /usr/local/bin/docker-compose down

    docker-sync stop
    if [[ $SYNC_CLEAN -eq 1 ]]; then
        echo " $(color.background.green "$(color.text.black "清除 sync 緩存")" )"
        docker-sync clean
    fi
}

docker.in(){
    /usr/local/bin/docker-compose exec ${1} ${2}
}
docker.build(){
    /usr/local/bin/docker-compose build
}
docker.restart(){
    /usr/local/bin/docker-compose restart "${@}"
}
docker.ps(){
    /usr/local/bin/docker ps
}
docker.compose.ps(){
    /usr/local/bin/docker-compose ps
}
docker.image(){
    /usr/local/bin/docker images
}
docker.log(){
    /usr/local/bin/docker-compose logs ${1}
}
