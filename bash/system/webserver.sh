#!/bin/bash
PATH_CONF_FOLDER="./nginx/sites"
PATH_HOSTS="/etc/hosts"

webserver.host() {
    local appendHost="127.0.0.1 ${1}"
    # 檢測hosts檔是否已新增
    sudo cat "${PATH_HOSTS}" | tr -s " " | grep -q "${appendHost}"
    # 找不到時，就新增至Hosts
    # Exit code 0        Success
    # Exit code 1        Errors
    if [ $? -eq 1 ]; then
        echo "${appendHost}"  | sudo tee -a $PATH_HOSTS
    fi
}

webserver.host.conf (){
    local projectName=${1}
    local domain=${2}
    local conf="${PATH_CONF_FOLDER}/${projectName}.conf"
    sudo cp "${PATH_CONF_FOLDER}/default.conf.example" $conf

    # if [ "${}" == "${LINUX}" ]; then
        # sudo sed -i "s#/var/www#/var/www/${projectName}/public#g" $conf
        # sudo sed -i "s/server_name localhost/server_name ${domain}/g" $conf
    # else
        sudo sed -i '' "s#/wwwroot#/wwwroot/${projectName}/public#g" $conf
        sudo sed -i '' "s/server_name localhost/server_name ${domain}/g" $conf
    # fi
}

