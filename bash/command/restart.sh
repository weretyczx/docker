#!/bin/bash
if [ "${2}" == "-f" ] ;then
    docker.down
    docker.up
else
    docker.restart ${2}
fi
