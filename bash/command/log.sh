#!/bin/bash
message="$(color.text.yellow "${2}:
")"
echo " $(color.background.black "
    $(color.text.white "Container log at ${message}")
")"
docker.log ${2}