#!/bin/bash
container=${2}
script='sh'
if [ -z $container ]; then
    container=$OS_CONTAINER
    script='zsh'
fi
docker.in $container $script