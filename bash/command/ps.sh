#!/bin/bash
message="$(color.text.yellow "docker ps:
")"
echo " $(color.background.black "
    $(color.text.white "Container info by ${message}")
")"
docker.ps

message="$(color.text.yellow "docker-compose ps:
")"
echo " $(color.background.black "
    $(color.text.white "Container info by ${message}")
")"
docker.compose.ps