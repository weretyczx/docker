#!/bin/bash
DOMAIN_SUFFIX=".laravel"
source $ROOT/bash/system/webserver.sh

# 列出專案
echo " $(color.text.green "請選擇需要發布的專案代碼:")"
projects=($( basename `ls -d ../*/`))
for index in "${projects[@]}"; do
    echo " $(color.text.black "${space} [${index}] ${projects[$index]}")"
done

# 取得輸入值
read -a answer
if  [ -z $answer ] ||                               # 非空值
    # ! [[ "$answer" =~ ^[0-9]+$ ]] ||              # 是數值
    [ "$answer" -gt "$(( ${#projects[@]} - 1 ))" ]  # 專案數以內
then
    echo " $(color.background.red "$(color.text.black "Error: 請輸入合法專案代碼")" )"
    exit
fi

# 設定 host 跟 nigx conf
project=${projects[answer]}
domain=$project
if ! [[ $project = *"."* ]]; then
  domain="${project}${DOMAIN_SUFFIX}"
fi
webserver.host $domain
webserver.host.conf $project $domain

# 啟動 docker
source $ROOT/bash/command/restart.sh "-f"
